﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text.RegularExpressions;
using WeatherEachDay.Models;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace WeatherEachDay
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        ObservableCollection<WeatherJson> WeatherEachHours;
        ObservableCollection<DailyForecast> WeatherEachDays;
        public MainPage()
        {
            this.InitializeComponent();

            WeatherEachHours = new ObservableCollection<WeatherJson>();
            InitJson();

            WeatherEachDays = new ObservableCollection<DailyForecast>();
            InitEachDaysJson();
        }

        private async void InitJson()
        {
            var url = "http://dataservice.accuweather.com/forecasts/v1/hourly/12hour/353412?" + "apikey=QUnERRYQt7dK8G0TGeGP5Q3saTWapm8L&language=vi-vn&metric=true";
            var list = await WeatherJson.GetJson(url) as List<WeatherJson>;
            Debug.WriteLine("Count: " + list.Count());

            list.ForEach(it =>
            {
                var match = Regex.Matches(it.DateTime, "T(?<time>\\d+):")[0].Groups["time"].Value;
                if (int.Parse(match) > 12)
                {
                    match = (int.Parse(match) - 12) + "PM";
                }
                else
                {
                    match += "PM";
                }
                it.DateTime = match;
                it.Temparature.Value += "\u00B0";
                it.WeatherIcon = string.Format("https://vortex.accuweather.com/adc2010/images/slate/icons/{0}.svg", it.WeatherIcon);

                WeatherEachHours.Add(it);
            }
            );
            WeatherDescTextBlock.Text = list[0].IconPhrase;
            WeatherTempTextBlock.Text = list[0].Temparature.Value;
        }

        private async void InitEachDaysJson()
        {
            var urlFiveDay = "http://dataservice.accuweather.com/forecasts/v1/daily/5day/353412?" + "apikey=QUnERRYQt7dK8G0TGeGP5Q3saTWapm8L&language=vi-vn&metric=true";
            var obj = await WeatherEachday.GetWeatherEach(urlFiveDay) as WeatherEachday;

            obj.DailyForecasts.ForEach(it =>
            {
                var matchs = Regex.Matches(it.Date, "\\d+");
                var date = new DateTime(int.Parse(matchs[0].Value), int.Parse(matchs[1].Value), int.Parse(matchs[2].Value));
                it.Date = date.DayOfWeek.ToString();

                it.Day.Icon = string.Format("https://vortex.accuweather.com/adc2010/images/slate/icons/{0}.svg", it.Day.Icon);
                Debug.WriteLine("Test Date: " + it.Date);
                WeatherEachDays.Add(it);

            }
            );

            Today.Text = WeatherEachDays[0].Date + " Today";
            MaxTemp.Text = WeatherEachDays[0].Temperature.Maximum.Value + "";
            MinTemp.Text = WeatherEachDays[0].Temperature.Minimum.Value + "";

            WeatherEachDays.RemoveAt(0);
        }

    }
}
