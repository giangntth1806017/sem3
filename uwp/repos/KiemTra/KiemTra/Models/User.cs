﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KiemTra.Models
{
    public class User
    {
        public string userName { get; set; }
        public string password { get; set; }
        public string name { get; set; }
        public string address { get; set; }
        public string country { get; set; }
        public string zipCode { get; set; }
        public string email { get; set; }
        public string sex { get; set; }
        public string language { get; set; }
        public string about { get; set; }
    }
}
