﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FakeNews.Model
{
    public class NewsItem
    {
        public int Id { get; set; }
        public string Category { get; set; }
        public string Headline { get; set; }
        public string Subhead { get; set; }
        public string DateLine { get; set; }
        public string Image { get; set; }
    }
    public class NewsManage
    {
        public static void GetNews(string Category, ObservableCollection<NewsItem> newsItems)
        {
            var allItems = getNewsItems();
            var filteredNewsItems = allItems.Where(p => p.Category == Category).ToList();
            newsItems.Clear();
            filteredNewsItems.ForEach(p => newsItems.Add(p));
        }
        private static List<NewsItem> getNewsItems()
        {
            var items = new List<NewsItem>();

            items.Add(new NewsItem() { Id = 1, Category = "Financial", DateLine = "29/06/1999", Headline = "Lorem Ipsum", Subhead = "Dumbledore", Image = "Assets/Financial.png" });
            items.Add(new NewsItem() { Id = 2, Category = "Financial", DateLine = "14/02/1995", Headline = "Cranked", Subhead = "Scout", Image = "Assets/Financial3.png" });
            items.Add(new NewsItem() { Id = 3, Category = "Financial", DateLine = "09/07/1993", Headline = "Red Bull", Subhead = "Gengar", Image = "Assets/Financial4.png" });
            items.Add(new NewsItem() { Id = 4, Category = "Financial", DateLine = "12/12/1992", Headline = "Absinthe", Subhead = "Alfred", Image = "Assets/Financial5.png" });
            items.Add(new NewsItem() { Id = 5, Category = "Food", DateLine = "03/06/1990", Headline = "Lou Will", Subhead = "Rivers", Image = "Assets/Food2.png" });
            items.Add(new NewsItem() { Id = 6, Category = "Food", DateLine = "11/08/1991", Headline = "Lebron", Subhead = "Curry", Image = "Assets/Food.png" });
            items.Add(new NewsItem() { Id = 7, Category = "Food", DateLine = "19/02/1992", Headline = "Smith", Subhead = "Klay", Image = "Assets/Food1.png" });
            items.Add(new NewsItem() { Id = 8, Category = "Food", DateLine = "27/9/1993", Headline = "Kyrie", Subhead = "Thompson", Image = "Assets/Food3.png" });
            items.Add(new NewsItem() { Id = 9, Category = "Food", DateLine = "12/10/1994", Headline = "Giannis", Subhead = "Bruce", Image = "Assets/Food4.png" });
            items.Add(new NewsItem() { Id = 10, Category = "Food", DateLine = "02/02/1997", Headline = "Docs", Subhead = "Alfred", Image = "Assets/Food5.png" });

            return items;
        }
    }
}
