﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;

namespace UWP_news.Models
{
    public class Source
    {
        public object id { get; set; }
        public string name { get; set; }
    }

    public class Article
    {
        public Source source { get; set; }
        public string author { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public string url { get; set; }
        public string urlToImage { get; set; }
        public string publishedAt { get; set; }
        public string content { get; set; }
    }

    public class RootObject
    {
        public string status { get; set; }
        public int totalResults { get; set; }
        public List<Article> articles { get; set; }
    }

    public class NewApi
    {
        public async static Task<RootObject> GetRootObjectNewApi()
        {
            var http = new HttpClient();
            var response = await http.GetAsync("https://newsapi.org/v2/everything?q=basketball&from=2019-10-01&sortBy=publishedAt&apiKey=a785c74218ce4778a1d6d675aa4c167d");
            var result = await response.Content.ReadAsStringAsync();
            var serializer = new DataContractJsonSerializer(typeof(RootObject));
            var ms = new MemoryStream(Encoding.UTF8.GetBytes(result));
            var value = serializer.ReadObject(ms) as RootObject;
            return value;
        }
    }
}
