﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace xBindData.Models
{
    public class Book
    {
        public string Title { get; set; }
        public string Author { get; set; }
        public string CoverImage { get; set; }
        public int BookId { get; set; }

        public Book(int v1, string v2, string v3, string v4)
        {
            this.BookId = v1;
            this.Title = v2;
            this.Author = v3;
            this.CoverImage = v4;
        }
    }

    public class BookManager
    {
        public static List<Book> GetBooks()
        {
            var books = new List<Book>();
            books.Add(new Book(1, "Vulpate", "Futurum", "Assets/1.png"));
            books.Add(new Book(2, "Mazim", "Erat", "Assets/2.png"));
            books.Add(new Book(3, "Elite", "Tempor", "Assets/3.png"));
            books.Add(new Book(4, "Itiam", "Option", "Assets/4.png"));
            books.Add(new Book(5, "Decima", "Imminat", "Assets/5.png"));
            books.Add(new Book(6, "Nostrut", "Zero", "Assets/6.png"));
            books.Add(new Book(7, "Penitent", "Gustave", "Assets/7.png"));
            books.Add(new Book(8, "Dio", "Hankel", "Assets/8.png"));
            books.Add(new Book(9, "Gracias", "Hennessy", "Assets/9.png"));
            books.Add(new Book(10, "Sorrow", "JR Smith", "Assets/10.png"));
            books.Add(new Book(11, "Silent", "Lebron", "Assets/11.png"));
            books.Add(new Book(12, "DecCrowima", "Durant", "Assets/12.png"));
            books.Add(new Book(13, "Russell", "Kyrie", "Assets/13.png"));
            return books;
        }
    }

}
