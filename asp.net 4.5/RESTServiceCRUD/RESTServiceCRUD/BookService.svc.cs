﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace RESTServiceCRUD
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "BookService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select BookService.svc or BookService.svc.cs at the Solution Explorer and start debugging.
    public class BookService : IBookService
    {
        static IBookRepo bookRepo = new BookRepo();
        public string AddBook(Book book)
        {
            Book newBook = bookRepo.AddNewBook(book);
            return "id = " + newBook.BookId;
        }

        public string DeleteBook(string id)
        {
            bool deleted = bookRepo.DeleteABook(int.Parse(id));
            if (deleted)
            {
                return "Book with id = " + id + " deleted successfully";
            }
            return "Unable to delete book with id = " + id;
        }

        public Book GetBookById(string id)
        {
            return bookRepo.GetBookById(int.Parse(id));
        }

        public List<Book> GetBooks()
        {
            return bookRepo.GetBooks();
        }

        public string UpdateBook(Book book, string id)
        {
            bool updated = bookRepo.UpdateABook(book);
            if (updated) {
                return "Book with id = " + id + " updated successfully";
            }
            return "Unable to update book with id = " + id;
        }
    }
}
