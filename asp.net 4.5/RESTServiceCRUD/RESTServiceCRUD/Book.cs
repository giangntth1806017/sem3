﻿using System.Web;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System;

namespace RESTServiceCRUD
{
    [DataContract]
    public class Book
    {
        [DataMember]
        public int BookId { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string ISBN { get; set; }
    }

    public interface IBookRepo
    {
        List<Book> GetBooks();
        Book GetBookById(int id);
        Book AddNewBook(Book item);
        bool DeleteABook(int id);
        bool UpdateABook(Book item);
    }

    public class BookRepo : IBookRepo
    {
        private List<Book> books = new List<Book>();
        private int count = 1;

        // constructor
        public BookRepo()
        {
            AddNewBook(new Book { Title = "Book 1", ISBN = "A01" });
            AddNewBook(new Book { Title = "Sach 2", ISBN = "B02" });
            AddNewBook(new Book { Title = "Book no 3", ISBN = "C03" });
            AddNewBook(new Book { Title = "Piece and War", ISBN = "HN2019" });
        }

        // create new
        public Book AddNewBook(Book item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("No new book");
            }
            item.BookId = count++;
            books.Add(item);
            return item;
        }

        // delete
        public bool DeleteABook(int id)
        {
            int index = books.FindIndex(b => b.BookId == id);
            if (index == -1)
            {
                return false;
            }
            books.RemoveAll(b => b.BookId == id);
            return true;
        }

        // lay book theo id
        public Book GetBookById(int id)
        {
            return books.Find(b => b.BookId == id);
        }

        // lay ve all book
        public List<Book> GetBooks()
        {
            return books;
        }

        // update
        public bool UpdateABook(Book item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("Update book");
            }
            int index = books.FindIndex(b => b.BookId == item.BookId);
            if (index == -1)
            {
                return false;
            }
            books.RemoveAt(index);
            books.Add(item);
            return true;
        }
    }
}