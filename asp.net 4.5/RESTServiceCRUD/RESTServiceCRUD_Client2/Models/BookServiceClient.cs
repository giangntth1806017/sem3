﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Script.Serialization;

namespace RESTServiceCRUD_Client.Models
{
    public class BookServiceClient
    {
        String BASE_URL = "http://localhost:58755/BookService.svc";

        // call get all books
        public List<Book> GetBooks()
        {
            var syncClient = new WebClient();
            var content = syncClient.DownloadString(BASE_URL + "/Books");
            var json_serializer = new JavaScriptSerializer();
            return json_serializer.Deserialize<List<Book>>(content);
        }

        // call get 1 book by id
        public Book GetBookById(string id)
        {
            var syncClient = new WebClient();
            var content = syncClient.DownloadString(BASE_URL + "/Book/" + id);
            var json_serializer = new JavaScriptSerializer();
            return json_serializer.Deserialize<Book>(content);
        }

        // add 1 book
        public string AddBook(Book book, int id)
        {
            var syncClient = new WebClient();
            var content = syncClient.DownloadString(BASE_URL + "/AddBook/" + id);re
            var json_serializer = new JavaScriptSerializer();
          //  return json_serializer.Deserialize<Book>(content);
        }
    }
}