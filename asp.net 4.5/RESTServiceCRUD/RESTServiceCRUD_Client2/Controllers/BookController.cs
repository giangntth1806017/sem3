﻿using RESTServiceCRUD_Client.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace RESTServiceCRUD_Client2.Controllers
{
    public class BookController : Controller
    {
        // GET: Book
        public ActionResult Index()
        {
            BookServiceClient bsc = new BookServiceClient();
            ViewBag.listBooks = bsc.GetBooks();
            return View();
        }

        // GET 1 Book by Id
        public ActionResult Detail(int? id)
        {
            BookServiceClient bsc = new BookServiceClient();
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Book book = bsc.GetBookById(id.ToString());
            if (book == null)
            {
                return HttpNotFound();
            }
            ViewBag.book = bsc.GetBookById(id.ToString());
            return View();
        }
    }
}