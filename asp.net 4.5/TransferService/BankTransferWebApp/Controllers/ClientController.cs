﻿using BankTransferWebApp.Models;
using BankTransferWebApp.ServiceReference1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace BankTransferWebApp.Controllers
{
    public class ClientController : Controller
    {
        // GET: Client
        public ActionResult Index()
        {
            ClientService clientService = new ClientService();
            ViewBag.listClient = clientService.GetClients();
            return View();
        }

        // GET: Client/Create
        public ActionResult Create()
        {
            ClientService clientService = new ClientService();
            return View();
        }

        // POST: Client/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Client_Acc,Client_Code,Client_Name,Client_Password,Deposit,Pin")] Client client)
        {
            ClientService clientService = new ClientService();
            TransferServiceClient webService = new TransferServiceClient();
            if (client != null)
            {
                var e = new Client
                {
                    Client_Acc = client.Client_Acc,
                    Client_Code = client.Client_Code,
                    Client_Name = client.Client_Name,
                    Client_Password = client.Client_Password,
                    Deposit = client.Deposit,
                    Pin = client.Pin
                };
                var returnClient = await webService.AddClientAsync(e);

                if (returnClient)
                {
                    return RedirectToAction("Index");
                }
            }
            return View(client);
        }
        //public ActionResult Create([Bind(Include = "Client_Acc,Client_Code,Client_Name,Client_Password,Deposit,Pin")] Client client)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        ClientService clientService = new ClientService();
        //        clientService.AddClient(client.ToString());
        //        return RedirectToAction("Index");
        //    }

        //    return View(client);
        //}

    }
}