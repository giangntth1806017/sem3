﻿using BankTransferWebApp.ServiceReference1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Script.Serialization;

namespace BankTransferWebApp.Models
{
    public class ClientService
    {
        String BASE_URL = "http://localhost:53889/TransferService.svc";

        // get all clients
        public List<Client> GetClients()
        {
            var syncClient = new WebClient();
            var content = syncClient.DownloadString(BASE_URL + "/Clients/");
            var json_serializer = new JavaScriptSerializer();
            return json_serializer.Deserialize<List<Client>>(content);
        }

        // add them 1 client
        public void AddClient(string param)
        {
            var syncClient = new WebClient();
            syncClient.Headers["Content-Type"] = "application/x-www-form-urlencoded";
            var content = syncClient.UploadString((BASE_URL + "/AddClient/"), param);
        }
    }
}