﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TransferService
{
	using System.Data.Linq;
	using System.Data.Linq.Mapping;
	using System.Data;
	using System.Collections.Generic;
	using System.Reflection;
	using System.Linq;
	using System.Linq.Expressions;
	using System.ComponentModel;
	using System;
	
	
	[global::System.Data.Linq.Mapping.DatabaseAttribute(Name="T1808M_NguyenTruongGiang")]
	public partial class TransferDataDataContext : System.Data.Linq.DataContext
	{
		
		private static System.Data.Linq.Mapping.MappingSource mappingSource = new AttributeMappingSource();
		
    #region Extensibility Method Definitions
    partial void OnCreated();
    partial void InsertClient(Client instance);
    partial void UpdateClient(Client instance);
    partial void DeleteClient(Client instance);
    partial void InsertDoitac(Doitac instance);
    partial void UpdateDoitac(Doitac instance);
    partial void DeleteDoitac(Doitac instance);
    partial void InsertTransferHistory(TransferHistory instance);
    partial void UpdateTransferHistory(TransferHistory instance);
    partial void DeleteTransferHistory(TransferHistory instance);
    #endregion
		
		public TransferDataDataContext() : 
				base(global::System.Configuration.ConfigurationManager.ConnectionStrings["T1808M_NguyenTruongGiangConnectionString"].ConnectionString, mappingSource)
		{
			OnCreated();
		}
		
		public TransferDataDataContext(string connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public TransferDataDataContext(System.Data.IDbConnection connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public TransferDataDataContext(string connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public TransferDataDataContext(System.Data.IDbConnection connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public System.Data.Linq.Table<Client> Clients
		{
			get
			{
				return this.GetTable<Client>();
			}
		}
		
		public System.Data.Linq.Table<Doitac> Doitacs
		{
			get
			{
				return this.GetTable<Doitac>();
			}
		}
		
		public System.Data.Linq.Table<TransferHistory> TransferHistories
		{
			get
			{
				return this.GetTable<TransferHistory>();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.Client")]
	public partial class Client : INotifyPropertyChanging, INotifyPropertyChanged
	{
		
		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
		
		private int _Client_Id;
		
		private string _Client_Name;
		
		private string _Client_Code;
		
		private string _Client_Acc;
		
		private System.Nullable<int> _Pin;
		
		private string _Client_Password;
		
		private System.Nullable<int> _Deposit;
		
		private System.Nullable<System.DateTime> _Created_At;
		
		private System.Nullable<System.DateTime> _Removed_At;
		
		private System.Nullable<System.DateTime> _Updated_At;
		
		private EntitySet<TransferHistory> _TransferHistories;
		
    #region Extensibility Method Definitions
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnClient_IdChanging(int value);
    partial void OnClient_IdChanged();
    partial void OnClient_NameChanging(string value);
    partial void OnClient_NameChanged();
    partial void OnClient_CodeChanging(string value);
    partial void OnClient_CodeChanged();
    partial void OnClient_AccChanging(string value);
    partial void OnClient_AccChanged();
    partial void OnPinChanging(System.Nullable<int> value);
    partial void OnPinChanged();
    partial void OnClient_PasswordChanging(string value);
    partial void OnClient_PasswordChanged();
    partial void OnDepositChanging(System.Nullable<int> value);
    partial void OnDepositChanged();
    partial void OnCreated_AtChanging(System.Nullable<System.DateTime> value);
    partial void OnCreated_AtChanged();
    partial void OnRemoved_AtChanging(System.Nullable<System.DateTime> value);
    partial void OnRemoved_AtChanged();
    partial void OnUpdated_AtChanging(System.Nullable<System.DateTime> value);
    partial void OnUpdated_AtChanged();
    #endregion
		
		public Client()
		{
			this._TransferHistories = new EntitySet<TransferHistory>(new Action<TransferHistory>(this.attach_TransferHistories), new Action<TransferHistory>(this.detach_TransferHistories));
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Client_Id", AutoSync=AutoSync.OnInsert, DbType="Int NOT NULL IDENTITY", IsPrimaryKey=true, IsDbGenerated=true)]
		public int Client_Id
		{
			get
			{
				return this._Client_Id;
			}
			set
			{
				if ((this._Client_Id != value))
				{
					this.OnClient_IdChanging(value);
					this.SendPropertyChanging();
					this._Client_Id = value;
					this.SendPropertyChanged("Client_Id");
					this.OnClient_IdChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Client_Name", DbType="NVarChar(250)")]
		public string Client_Name
		{
			get
			{
				return this._Client_Name;
			}
			set
			{
				if ((this._Client_Name != value))
				{
					this.OnClient_NameChanging(value);
					this.SendPropertyChanging();
					this._Client_Name = value;
					this.SendPropertyChanged("Client_Name");
					this.OnClient_NameChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Client_Code", DbType="NVarChar(50)")]
		public string Client_Code
		{
			get
			{
				return this._Client_Code;
			}
			set
			{
				if ((this._Client_Code != value))
				{
					this.OnClient_CodeChanging(value);
					this.SendPropertyChanging();
					this._Client_Code = value;
					this.SendPropertyChanged("Client_Code");
					this.OnClient_CodeChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Client_Acc", DbType="NVarChar(250)")]
		public string Client_Acc
		{
			get
			{
				return this._Client_Acc;
			}
			set
			{
				if ((this._Client_Acc != value))
				{
					this.OnClient_AccChanging(value);
					this.SendPropertyChanging();
					this._Client_Acc = value;
					this.SendPropertyChanged("Client_Acc");
					this.OnClient_AccChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Pin", DbType="Int")]
		public System.Nullable<int> Pin
		{
			get
			{
				return this._Pin;
			}
			set
			{
				if ((this._Pin != value))
				{
					this.OnPinChanging(value);
					this.SendPropertyChanging();
					this._Pin = value;
					this.SendPropertyChanged("Pin");
					this.OnPinChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Client_Password", DbType="NVarChar(50)")]
		public string Client_Password
		{
			get
			{
				return this._Client_Password;
			}
			set
			{
				if ((this._Client_Password != value))
				{
					this.OnClient_PasswordChanging(value);
					this.SendPropertyChanging();
					this._Client_Password = value;
					this.SendPropertyChanged("Client_Password");
					this.OnClient_PasswordChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Deposit", DbType="Int")]
		public System.Nullable<int> Deposit
		{
			get
			{
				return this._Deposit;
			}
			set
			{
				if ((this._Deposit != value))
				{
					this.OnDepositChanging(value);
					this.SendPropertyChanging();
					this._Deposit = value;
					this.SendPropertyChanged("Deposit");
					this.OnDepositChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Created_At", DbType="DateTime")]
		public System.Nullable<System.DateTime> Created_At
		{
			get
			{
				return this._Created_At;
			}
			set
			{
				if ((this._Created_At != value))
				{
					this.OnCreated_AtChanging(value);
					this.SendPropertyChanging();
					this._Created_At = value;
					this.SendPropertyChanged("Created_At");
					this.OnCreated_AtChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Removed_At", DbType="DateTime")]
		public System.Nullable<System.DateTime> Removed_At
		{
			get
			{
				return this._Removed_At;
			}
			set
			{
				if ((this._Removed_At != value))
				{
					this.OnRemoved_AtChanging(value);
					this.SendPropertyChanging();
					this._Removed_At = value;
					this.SendPropertyChanged("Removed_At");
					this.OnRemoved_AtChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Updated_At", DbType="DateTime")]
		public System.Nullable<System.DateTime> Updated_At
		{
			get
			{
				return this._Updated_At;
			}
			set
			{
				if ((this._Updated_At != value))
				{
					this.OnUpdated_AtChanging(value);
					this.SendPropertyChanging();
					this._Updated_At = value;
					this.SendPropertyChanged("Updated_At");
					this.OnUpdated_AtChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.AssociationAttribute(Name="Client_TransferHistory", Storage="_TransferHistories", ThisKey="Client_Id", OtherKey="Client_Id")]
		public EntitySet<TransferHistory> TransferHistories
		{
			get
			{
				return this._TransferHistories;
			}
			set
			{
				this._TransferHistories.Assign(value);
			}
		}
		
		public event PropertyChangingEventHandler PropertyChanging;
		
		public event PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			if ((this.PropertyChanging != null))
			{
				this.PropertyChanging(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(String propertyName)
		{
			if ((this.PropertyChanged != null))
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
		
		private void attach_TransferHistories(TransferHistory entity)
		{
			this.SendPropertyChanging();
			entity.Client = this;
		}
		
		private void detach_TransferHistories(TransferHistory entity)
		{
			this.SendPropertyChanging();
			entity.Client = null;
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.Doitac")]
	public partial class Doitac : INotifyPropertyChanging, INotifyPropertyChanged
	{
		
		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
		
		private int _Doitac_Id;
		
		private string _Doitac_Name;
		
		private string _Doitac_Code;
		
		private string _Doitac_Acc;
		
		private System.Nullable<int> _Pin;
		
		private string _Doitac_Password;
		
		private System.Nullable<int> _Deposit;
		
		private System.Nullable<System.DateTime> _Created_At;
		
		private System.Nullable<System.DateTime> _Removed_At;
		
		private System.Nullable<System.DateTime> _Updated_At;
		
		private EntitySet<TransferHistory> _TransferHistories;
		
    #region Extensibility Method Definitions
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnDoitac_IdChanging(int value);
    partial void OnDoitac_IdChanged();
    partial void OnDoitac_NameChanging(string value);
    partial void OnDoitac_NameChanged();
    partial void OnDoitac_CodeChanging(string value);
    partial void OnDoitac_CodeChanged();
    partial void OnDoitac_AccChanging(string value);
    partial void OnDoitac_AccChanged();
    partial void OnPinChanging(System.Nullable<int> value);
    partial void OnPinChanged();
    partial void OnDoitac_PasswordChanging(string value);
    partial void OnDoitac_PasswordChanged();
    partial void OnDepositChanging(System.Nullable<int> value);
    partial void OnDepositChanged();
    partial void OnCreated_AtChanging(System.Nullable<System.DateTime> value);
    partial void OnCreated_AtChanged();
    partial void OnRemoved_AtChanging(System.Nullable<System.DateTime> value);
    partial void OnRemoved_AtChanged();
    partial void OnUpdated_AtChanging(System.Nullable<System.DateTime> value);
    partial void OnUpdated_AtChanged();
    #endregion
		
		public Doitac()
		{
			this._TransferHistories = new EntitySet<TransferHistory>(new Action<TransferHistory>(this.attach_TransferHistories), new Action<TransferHistory>(this.detach_TransferHistories));
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Doitac_Id", AutoSync=AutoSync.OnInsert, DbType="Int NOT NULL IDENTITY", IsPrimaryKey=true, IsDbGenerated=true)]
		public int Doitac_Id
		{
			get
			{
				return this._Doitac_Id;
			}
			set
			{
				if ((this._Doitac_Id != value))
				{
					this.OnDoitac_IdChanging(value);
					this.SendPropertyChanging();
					this._Doitac_Id = value;
					this.SendPropertyChanged("Doitac_Id");
					this.OnDoitac_IdChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Doitac_Name", DbType="NVarChar(250)")]
		public string Doitac_Name
		{
			get
			{
				return this._Doitac_Name;
			}
			set
			{
				if ((this._Doitac_Name != value))
				{
					this.OnDoitac_NameChanging(value);
					this.SendPropertyChanging();
					this._Doitac_Name = value;
					this.SendPropertyChanged("Doitac_Name");
					this.OnDoitac_NameChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Doitac_Code", DbType="NVarChar(50)")]
		public string Doitac_Code
		{
			get
			{
				return this._Doitac_Code;
			}
			set
			{
				if ((this._Doitac_Code != value))
				{
					this.OnDoitac_CodeChanging(value);
					this.SendPropertyChanging();
					this._Doitac_Code = value;
					this.SendPropertyChanged("Doitac_Code");
					this.OnDoitac_CodeChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Doitac_Acc", DbType="NVarChar(250)")]
		public string Doitac_Acc
		{
			get
			{
				return this._Doitac_Acc;
			}
			set
			{
				if ((this._Doitac_Acc != value))
				{
					this.OnDoitac_AccChanging(value);
					this.SendPropertyChanging();
					this._Doitac_Acc = value;
					this.SendPropertyChanged("Doitac_Acc");
					this.OnDoitac_AccChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Pin", DbType="Int")]
		public System.Nullable<int> Pin
		{
			get
			{
				return this._Pin;
			}
			set
			{
				if ((this._Pin != value))
				{
					this.OnPinChanging(value);
					this.SendPropertyChanging();
					this._Pin = value;
					this.SendPropertyChanged("Pin");
					this.OnPinChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Doitac_Password", DbType="NVarChar(50)")]
		public string Doitac_Password
		{
			get
			{
				return this._Doitac_Password;
			}
			set
			{
				if ((this._Doitac_Password != value))
				{
					this.OnDoitac_PasswordChanging(value);
					this.SendPropertyChanging();
					this._Doitac_Password = value;
					this.SendPropertyChanged("Doitac_Password");
					this.OnDoitac_PasswordChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Deposit", DbType="Int")]
		public System.Nullable<int> Deposit
		{
			get
			{
				return this._Deposit;
			}
			set
			{
				if ((this._Deposit != value))
				{
					this.OnDepositChanging(value);
					this.SendPropertyChanging();
					this._Deposit = value;
					this.SendPropertyChanged("Deposit");
					this.OnDepositChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Created_At", DbType="DateTime")]
		public System.Nullable<System.DateTime> Created_At
		{
			get
			{
				return this._Created_At;
			}
			set
			{
				if ((this._Created_At != value))
				{
					this.OnCreated_AtChanging(value);
					this.SendPropertyChanging();
					this._Created_At = value;
					this.SendPropertyChanged("Created_At");
					this.OnCreated_AtChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Removed_At", DbType="DateTime")]
		public System.Nullable<System.DateTime> Removed_At
		{
			get
			{
				return this._Removed_At;
			}
			set
			{
				if ((this._Removed_At != value))
				{
					this.OnRemoved_AtChanging(value);
					this.SendPropertyChanging();
					this._Removed_At = value;
					this.SendPropertyChanged("Removed_At");
					this.OnRemoved_AtChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Updated_At", DbType="DateTime")]
		public System.Nullable<System.DateTime> Updated_At
		{
			get
			{
				return this._Updated_At;
			}
			set
			{
				if ((this._Updated_At != value))
				{
					this.OnUpdated_AtChanging(value);
					this.SendPropertyChanging();
					this._Updated_At = value;
					this.SendPropertyChanged("Updated_At");
					this.OnUpdated_AtChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.AssociationAttribute(Name="Doitac_TransferHistory", Storage="_TransferHistories", ThisKey="Doitac_Id", OtherKey="Doitac_Id")]
		public EntitySet<TransferHistory> TransferHistories
		{
			get
			{
				return this._TransferHistories;
			}
			set
			{
				this._TransferHistories.Assign(value);
			}
		}
		
		public event PropertyChangingEventHandler PropertyChanging;
		
		public event PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			if ((this.PropertyChanging != null))
			{
				this.PropertyChanging(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(String propertyName)
		{
			if ((this.PropertyChanged != null))
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
		
		private void attach_TransferHistories(TransferHistory entity)
		{
			this.SendPropertyChanging();
			entity.Doitac = this;
		}
		
		private void detach_TransferHistories(TransferHistory entity)
		{
			this.SendPropertyChanging();
			entity.Doitac = null;
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.TransferHistory")]
	public partial class TransferHistory : INotifyPropertyChanging, INotifyPropertyChanged
	{
		
		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
		
		private int _Transfer_Id;
		
		private string _Transfer_Name;
		
		private string _Transfer_Code;
		
		private System.Nullable<int> _Transfer_Amount;
		
		private System.Nullable<int> _Transfer_Type;
		
		private System.Nullable<int> _Client_Id;
		
		private System.Nullable<int> _Doitac_Id;
		
		private string _Client_Code;
		
		private string _Doitac_Code;
		
		private string _Order_Code;
		
		private System.Nullable<int> _Fee;
		
		private System.Nullable<int> _SoTien;
		
		private System.Nullable<System.DateTime> _Created_At;
		
		private System.Nullable<System.DateTime> _Removed_At;
		
		private System.Nullable<System.DateTime> _Updated_At;
		
		private EntityRef<Client> _Client;
		
		private EntityRef<Doitac> _Doitac;
		
    #region Extensibility Method Definitions
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnTransfer_IdChanging(int value);
    partial void OnTransfer_IdChanged();
    partial void OnTransfer_NameChanging(string value);
    partial void OnTransfer_NameChanged();
    partial void OnTransfer_CodeChanging(string value);
    partial void OnTransfer_CodeChanged();
    partial void OnTransfer_AmountChanging(System.Nullable<int> value);
    partial void OnTransfer_AmountChanged();
    partial void OnTransfer_TypeChanging(System.Nullable<int> value);
    partial void OnTransfer_TypeChanged();
    partial void OnClient_IdChanging(System.Nullable<int> value);
    partial void OnClient_IdChanged();
    partial void OnDoitac_IdChanging(System.Nullable<int> value);
    partial void OnDoitac_IdChanged();
    partial void OnClient_CodeChanging(string value);
    partial void OnClient_CodeChanged();
    partial void OnDoitac_CodeChanging(string value);
    partial void OnDoitac_CodeChanged();
    partial void OnOrder_CodeChanging(string value);
    partial void OnOrder_CodeChanged();
    partial void OnFeeChanging(System.Nullable<int> value);
    partial void OnFeeChanged();
    partial void OnSoTienChanging(System.Nullable<int> value);
    partial void OnSoTienChanged();
    partial void OnCreated_AtChanging(System.Nullable<System.DateTime> value);
    partial void OnCreated_AtChanged();
    partial void OnRemoved_AtChanging(System.Nullable<System.DateTime> value);
    partial void OnRemoved_AtChanged();
    partial void OnUpdated_AtChanging(System.Nullable<System.DateTime> value);
    partial void OnUpdated_AtChanged();
    #endregion
		
		public TransferHistory()
		{
			this._Client = default(EntityRef<Client>);
			this._Doitac = default(EntityRef<Doitac>);
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Transfer_Id", AutoSync=AutoSync.OnInsert, DbType="Int NOT NULL IDENTITY", IsPrimaryKey=true, IsDbGenerated=true)]
		public int Transfer_Id
		{
			get
			{
				return this._Transfer_Id;
			}
			set
			{
				if ((this._Transfer_Id != value))
				{
					this.OnTransfer_IdChanging(value);
					this.SendPropertyChanging();
					this._Transfer_Id = value;
					this.SendPropertyChanged("Transfer_Id");
					this.OnTransfer_IdChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Transfer_Name", DbType="NVarChar(250)")]
		public string Transfer_Name
		{
			get
			{
				return this._Transfer_Name;
			}
			set
			{
				if ((this._Transfer_Name != value))
				{
					this.OnTransfer_NameChanging(value);
					this.SendPropertyChanging();
					this._Transfer_Name = value;
					this.SendPropertyChanged("Transfer_Name");
					this.OnTransfer_NameChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Transfer_Code", DbType="NVarChar(50)")]
		public string Transfer_Code
		{
			get
			{
				return this._Transfer_Code;
			}
			set
			{
				if ((this._Transfer_Code != value))
				{
					this.OnTransfer_CodeChanging(value);
					this.SendPropertyChanging();
					this._Transfer_Code = value;
					this.SendPropertyChanged("Transfer_Code");
					this.OnTransfer_CodeChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Transfer_Amount", DbType="Int")]
		public System.Nullable<int> Transfer_Amount
		{
			get
			{
				return this._Transfer_Amount;
			}
			set
			{
				if ((this._Transfer_Amount != value))
				{
					this.OnTransfer_AmountChanging(value);
					this.SendPropertyChanging();
					this._Transfer_Amount = value;
					this.SendPropertyChanged("Transfer_Amount");
					this.OnTransfer_AmountChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Transfer_Type", DbType="Int")]
		public System.Nullable<int> Transfer_Type
		{
			get
			{
				return this._Transfer_Type;
			}
			set
			{
				if ((this._Transfer_Type != value))
				{
					this.OnTransfer_TypeChanging(value);
					this.SendPropertyChanging();
					this._Transfer_Type = value;
					this.SendPropertyChanged("Transfer_Type");
					this.OnTransfer_TypeChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Client_Id", DbType="Int")]
		public System.Nullable<int> Client_Id
		{
			get
			{
				return this._Client_Id;
			}
			set
			{
				if ((this._Client_Id != value))
				{
					if (this._Client.HasLoadedOrAssignedValue)
					{
						throw new System.Data.Linq.ForeignKeyReferenceAlreadyHasValueException();
					}
					this.OnClient_IdChanging(value);
					this.SendPropertyChanging();
					this._Client_Id = value;
					this.SendPropertyChanged("Client_Id");
					this.OnClient_IdChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Doitac_Id", DbType="Int")]
		public System.Nullable<int> Doitac_Id
		{
			get
			{
				return this._Doitac_Id;
			}
			set
			{
				if ((this._Doitac_Id != value))
				{
					if (this._Doitac.HasLoadedOrAssignedValue)
					{
						throw new System.Data.Linq.ForeignKeyReferenceAlreadyHasValueException();
					}
					this.OnDoitac_IdChanging(value);
					this.SendPropertyChanging();
					this._Doitac_Id = value;
					this.SendPropertyChanged("Doitac_Id");
					this.OnDoitac_IdChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Client_Code", DbType="NVarChar(50)")]
		public string Client_Code
		{
			get
			{
				return this._Client_Code;
			}
			set
			{
				if ((this._Client_Code != value))
				{
					this.OnClient_CodeChanging(value);
					this.SendPropertyChanging();
					this._Client_Code = value;
					this.SendPropertyChanged("Client_Code");
					this.OnClient_CodeChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Doitac_Code", DbType="NVarChar(50)")]
		public string Doitac_Code
		{
			get
			{
				return this._Doitac_Code;
			}
			set
			{
				if ((this._Doitac_Code != value))
				{
					this.OnDoitac_CodeChanging(value);
					this.SendPropertyChanging();
					this._Doitac_Code = value;
					this.SendPropertyChanged("Doitac_Code");
					this.OnDoitac_CodeChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Order_Code", DbType="NVarChar(50)")]
		public string Order_Code
		{
			get
			{
				return this._Order_Code;
			}
			set
			{
				if ((this._Order_Code != value))
				{
					this.OnOrder_CodeChanging(value);
					this.SendPropertyChanging();
					this._Order_Code = value;
					this.SendPropertyChanged("Order_Code");
					this.OnOrder_CodeChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Fee", DbType="Int")]
		public System.Nullable<int> Fee
		{
			get
			{
				return this._Fee;
			}
			set
			{
				if ((this._Fee != value))
				{
					this.OnFeeChanging(value);
					this.SendPropertyChanging();
					this._Fee = value;
					this.SendPropertyChanged("Fee");
					this.OnFeeChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_SoTien", DbType="Int")]
		public System.Nullable<int> SoTien
		{
			get
			{
				return this._SoTien;
			}
			set
			{
				if ((this._SoTien != value))
				{
					this.OnSoTienChanging(value);
					this.SendPropertyChanging();
					this._SoTien = value;
					this.SendPropertyChanged("SoTien");
					this.OnSoTienChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Created_At", DbType="DateTime")]
		public System.Nullable<System.DateTime> Created_At
		{
			get
			{
				return this._Created_At;
			}
			set
			{
				if ((this._Created_At != value))
				{
					this.OnCreated_AtChanging(value);
					this.SendPropertyChanging();
					this._Created_At = value;
					this.SendPropertyChanged("Created_At");
					this.OnCreated_AtChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Removed_At", DbType="DateTime")]
		public System.Nullable<System.DateTime> Removed_At
		{
			get
			{
				return this._Removed_At;
			}
			set
			{
				if ((this._Removed_At != value))
				{
					this.OnRemoved_AtChanging(value);
					this.SendPropertyChanging();
					this._Removed_At = value;
					this.SendPropertyChanged("Removed_At");
					this.OnRemoved_AtChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Updated_At", DbType="DateTime")]
		public System.Nullable<System.DateTime> Updated_At
		{
			get
			{
				return this._Updated_At;
			}
			set
			{
				if ((this._Updated_At != value))
				{
					this.OnUpdated_AtChanging(value);
					this.SendPropertyChanging();
					this._Updated_At = value;
					this.SendPropertyChanged("Updated_At");
					this.OnUpdated_AtChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.AssociationAttribute(Name="Client_TransferHistory", Storage="_Client", ThisKey="Client_Id", OtherKey="Client_Id", IsForeignKey=true)]
		public Client Client
		{
			get
			{
				return this._Client.Entity;
			}
			set
			{
				Client previousValue = this._Client.Entity;
				if (((previousValue != value) 
							|| (this._Client.HasLoadedOrAssignedValue == false)))
				{
					this.SendPropertyChanging();
					if ((previousValue != null))
					{
						this._Client.Entity = null;
						previousValue.TransferHistories.Remove(this);
					}
					this._Client.Entity = value;
					if ((value != null))
					{
						value.TransferHistories.Add(this);
						this._Client_Id = value.Client_Id;
					}
					else
					{
						this._Client_Id = default(Nullable<int>);
					}
					this.SendPropertyChanged("Client");
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.AssociationAttribute(Name="Doitac_TransferHistory", Storage="_Doitac", ThisKey="Doitac_Id", OtherKey="Doitac_Id", IsForeignKey=true)]
		public Doitac Doitac
		{
			get
			{
				return this._Doitac.Entity;
			}
			set
			{
				Doitac previousValue = this._Doitac.Entity;
				if (((previousValue != value) 
							|| (this._Doitac.HasLoadedOrAssignedValue == false)))
				{
					this.SendPropertyChanging();
					if ((previousValue != null))
					{
						this._Doitac.Entity = null;
						previousValue.TransferHistories.Remove(this);
					}
					this._Doitac.Entity = value;
					if ((value != null))
					{
						value.TransferHistories.Add(this);
						this._Doitac_Id = value.Doitac_Id;
					}
					else
					{
						this._Doitac_Id = default(Nullable<int>);
					}
					this.SendPropertyChanged("Doitac");
				}
			}
		}
		
		public event PropertyChangingEventHandler PropertyChanging;
		
		public event PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			if ((this.PropertyChanging != null))
			{
				this.PropertyChanging(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(String propertyName)
		{
			if ((this.PropertyChanged != null))
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
	}
}
#pragma warning restore 1591
