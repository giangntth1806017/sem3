﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace TransferService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "TransferService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select TransferService.svc or TransferService.svc.cs at the Solution Explorer and start debugging.
    public class TransferService : ITransferService
    {
        // data context
        TransferDataDataContext data = new TransferDataDataContext();

        public bool AddClient(Client client)
        {
            try
            {
                data.Clients.InsertOnSubmit(client);
                data.SubmitChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool AddDoiTac(Doitac doitac)
        {
            try
            {
                data.Doitacs.InsertOnSubmit(doitac);
                data.SubmitChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool AddTransfer(TransferHistory transferHistory)
        {
            try
            {
                data.TransferHistories.InsertOnSubmit(transferHistory);
                data.SubmitChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public List<Client> GetClients()
        {
            try
            {
                return (from client in data.Clients select client).ToList();
            }
            catch
            {
                return null;
            }
        }

        public List<Doitac> GetDoitacs()
        {
            try
            {
                return (from doitac in data.Doitacs select doitac).ToList();
            }
            catch
            {
                return null;
            }
        }

        public List<TransferHistory> GetTransferHistories()
        {
            try
            {
                return (from lichsu in data.TransferHistories select lichsu).ToList();
            }
            catch
            {
                return null;
            }
        }
    }
}
