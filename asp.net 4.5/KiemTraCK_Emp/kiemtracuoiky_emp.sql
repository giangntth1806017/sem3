create table EMPLOYEE (
ID int identity not null,
EMPLOYEEID varchar(10) not null,
EMPLOYEENAME varchar(255) not null,
DEPARTMENT varchar(50) not null,
SALARY int not null,
primary key (ID)
);