﻿using System;
using System.Collections.Generic;
using System.Data.Linq.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace KiemTraWCF
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "EmployeeService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select EmployeeService.svc or EmployeeService.svc.cs at the Solution Explorer and start debugging.
    public class EmployeeService : IEmployeeService
    {
        EmployeeDataDataContext data = new EmployeeDataDataContext();
        public bool AddEmployee(NhanVien nhanVien)
        {
            try
            {
                data.NhanViens.InsertOnSubmit(nhanVien);
                data.SubmitChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public List<NhanVien> GetEmpByDep(string department)
        {
            try
            {
                return (from emp in data.NhanViens where emp.Department.Contains(department) select emp).ToList();
            }
            catch
            {
                return null;
            }
        }

        public List<NhanVien> GetNhanViens()
        {
            try
            {
                return (from emp in data.NhanViens select emp).ToList();
            }
            catch
            {
                return null;
            }
        }

        public List<NhanVien> SearchEmplyeeByDepartmentName(string name)
        {
            try
            {
                List<NhanVien> results =
                    (from e in data.NhanViens where SqlMethods.Like(e.Department, "%" + name + "%") select e).ToList();
                return results;
            }
            catch
            {
                return null;
            }
        }

    }
}
