﻿using EmpService_Client.ServiceReference1;
using KiemTraWCF;
using System;
using System.Collections.Generic;
using System.Data.Linq.SqlClient;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Script.Serialization;

namespace EmpService_Client.Models
{
    public class EmpServiceClient
    {
        EmployeeDataDataContext data = new EmployeeDataDataContext();
        String BASE_URL = "http://localhost:63731/EmployeeService.svc";

        public List<NhanVien> GetNhanViens()
        {
            var syncClient = new WebClient();
            var content = syncClient.DownloadString(BASE_URL + "/GetEmployees/");
            var json_serializer = new JavaScriptSerializer();
            return json_serializer.Deserialize<List<NhanVien>>(content);
        }

        public List<NhanVien> GetNhanViensByDepartment(string department)
        {
            var syncClient = new WebClient();
            var content = syncClient.DownloadString(BASE_URL + "/GetEmpByDep/" + department.Trim());
            var json_serializer = new JavaScriptSerializer();
            return json_serializer.Deserialize<List<NhanVien>>(content);
        }

        internal dynamic SearchEmplyeeByDepartmentName(string name)
        {
            try
            {
                List<NhanVien> results =
                    (from e in data.NhanViens where SqlMethods.Like(e.Department, "%" + name + "%") select e).ToList();
                return results;
            }
            catch
            {
                return null;
            }
        }
    }
}