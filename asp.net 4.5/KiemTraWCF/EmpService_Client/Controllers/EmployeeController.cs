﻿using EmpService_Client.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EmpService_Client.Controllers
{
    public class EmployeeController : Controller
    {
        EmpServiceClient service = new EmpServiceClient();
        // GET: Employee
        public ActionResult Index()
        {
            EmpServiceClient service = new EmpServiceClient();
            ViewBag.listEmp = service.GetNhanViens();
            return View();
        }

        // GET: Employee from department
        public ActionResult Search()
        {
            ViewBag.employee = null;
            return View();
        }

        [HttpPost]
        public ActionResult Search(Models.SearchModel searchData)
        {
            try
            {
                ViewBag.employee = service.SearchEmplyeeByDepartmentName(searchData.name);
                return View();
            }
            catch
            {
                ViewBag.employee = null;
                return View();
            }
        }


        // Create: Employee
        public ActionResult Create()
        {
            return View();
        }
    }
}