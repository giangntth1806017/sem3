﻿using AzureClientApp.EmployeeService;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace AzureClientApp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        EmployeeService.EmployeeServiceClient webService = new EmployeeService.EmployeeServiceClient();
        public MainPage()
        {
            this.InitializeComponent();
            this.Loaded += MainPage_Loaded;
        }
        private void MainPage_Loaded(object sender, RoutedEventArgs e)
        {
            getEmployee();
        }
        private async void getEmployee()
        {
            try
            {
                ProgressBar.IsIndeterminate = true;
                ProgressBar.Visibility = Visibility.Visible;

                GridViewEmployee.ItemsSource = await webService.getEmployeesAsync();
                ProgressBar.Visibility = Visibility.Collapsed;
                ProgressBar.IsIndeterminate = false;
            }
            catch (Exception e)
            {
                ShowMessage(e.Message);
                ProgressBar.Visibility = Visibility.Collapsed;
                ProgressBar.IsIndeterminate = false;
            }
        }

        private void GridViewEmployee_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (e.AddedItems.Count != 0)
                {
                    var selectedEmployee = e.AddedItems[0] as EmployeeService.Employee;
                    TextBoxName.Text = selectedEmployee.FirstName;
                    TextBoxSurName.Text = selectedEmployee.LastName;
                    TextBoxAddress.Text = selectedEmployee.Address;
                    TextBoxAge.Text = selectedEmployee.Age.ToString();
                }

            }
            catch (Exception ex)
            {
                ShowMessage("Error Data!!");
                ProgressBar.Visibility = Visibility.Collapsed;
                ProgressBar.IsIndeterminate = false;
            }
        }

        private async void ButtonAdd_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ProgressBar.IsIndeterminate = true;
                ProgressBar.Visibility = Visibility.Visible;
                var employee = new Employee
                {
                    FirstName = TextBoxName.Text,
                    LastName = TextBoxSurName.Text,
                    Address = TextBoxAddress.Text,
                    Age = Convert.ToInt32(TextBoxAge.Text),
                };

                var result = await webService.addNewCustomerAsync(employee);
                ProgressBar.Visibility = Visibility.Collapsed;
                ProgressBar.IsIndeterminate = false;

                if (result)
                {
                    ShowMessage("Inserted successful");
                    Reset();
                }
                else
                {
                    ShowMessage("Can't insert");
                }
                getEmployee();
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message);

            }
        }

        private void Reset()
        {
            TextBoxAddress.Text = string.Empty;
            TextBoxAge.Text = string.Empty;
            TextBoxName.Text = string.Empty;
            TextBoxSurName.Text = string.Empty;
        }
        private async void ButtonDelete_Click(object sender, RoutedEventArgs e)
        {
            if (GridViewEmployee.SelectedItem != null)
            {
                try
                {
                    ProgressBar.IsIndeterminate = true;
                    ProgressBar.Visibility = Visibility.Visible;

                    var result = await webService.deleteEmployeeAsync((GridViewEmployee.SelectedItem as Employee).empID);

                    if (result)
                    {
                        ShowMessage("Deleted successful");
                        Reset();
                    }
                    else
                    {
                        ShowMessage("Can't delete");
                    }
                    getEmployee();
                }
                catch (Exception ex)
                {
                    ShowMessage(ex.Message);
                    ProgressBar.Visibility = Visibility.Collapsed;
                    ProgressBar.IsIndeterminate = false;

                }
            }
            else
            {
                ShowMessage("Choose record to delete!");
            }
        }

        private async void ButtonModify_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ProgressBar.IsIndeterminate = true;
                ProgressBar.Visibility = Visibility.Visible;
                var employee = new Employee
                {
                    empID = (GridViewEmployee.SelectedItem as Employee).empID,
                    FirstName = TextBoxName.Text,
                    LastName = TextBoxSurName.Text,
                    Address = TextBoxAddress.Text,
                    Age = Convert.ToInt32(TextBoxAge.Text)
                };

                var result = await webService.modifyEmployeeAsync(employee);

                ProgressBar.Visibility = Visibility.Collapsed;
                ProgressBar.IsIndeterminate = false;
                if (result)
                {
                    ShowMessage("Modify successful");
                    Reset();
                }
                else
                {
                    ShowMessage("Can't modify");
                }
                getEmployee();
            }
            catch (Exception ex)
            {
                ShowMessage("Choose employee!");
                ProgressBar.Visibility = Visibility.Collapsed;
                ProgressBar.IsIndeterminate = false;
            }
        }
        private async void ShowMessage(string message)
        {
            var messageDialog = new MessageDialog(message);
            await messageDialog.ShowAsync();
        }
    }
}
