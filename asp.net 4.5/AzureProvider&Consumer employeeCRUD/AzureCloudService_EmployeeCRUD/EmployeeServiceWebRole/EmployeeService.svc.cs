﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace EmployeeServiceWebRole
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "EmployeeService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select EmployeeService.svc or EmployeeService.svc.cs at the Solution Explorer and start debugging.
    public class EmployeeService : IEmployeeService
    {
        EmployeeDataDataContext data = new EmployeeDataDataContext();

        public bool addNewCustomer(Employee employee)
        {
            try
            {
                data.Employees.InsertOnSubmit(employee);
                data.SubmitChanges();
                return true;

            }
            catch (Exception e)
            {
                return false;
            }

        }

        public bool deleteEmployee(int employeeID)
        {
            try
            {
                var e = data.Employees.Where(it => it.empID == employeeID).Single();

                if (e != null)
                {
                    data.Employees.DeleteOnSubmit(e);
                    data.SubmitChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public List<Employee> getEmployees()
        {
            try
            {
                return data.Employees.ToList();
            }
            catch (Exception ex)
            {
                return new List<Employee>();
            }
        }

        public bool modifyEmployee(Employee newEmployee)
        {
            try
            {
                var e = data.Employees.Where(it => it.empID == newEmployee.empID).Single();
                if (e != null)
                {
                    e.Address = newEmployee.Address;
                    e.Age = newEmployee.Age;
                    e.FirstName = newEmployee.FirstName;
                    e.LastName = newEmployee.LastName;

                    data.SubmitChanges();

                    return true;
                }
            }
            catch (Exception ex)
            {
            }
            return false;

        }
    }
}
