﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Linq;
using System.Collections.Generic;

namespace EmployeeWcfServiceREST
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class EmployeeNew : IEmployee
    {
        EmployeeDataDataContext data = new EmployeeDataDataContext();
        public bool AddEmployee(Employee eml)
        {
            try
            {
                data.Employees.InsertOnSubmit(eml);
                data.SubmitChanges();
                return true;
            }
            catch 
            {

                return false;
            }
            //throw new NotImplementedException();
        }

        public bool DeleteEmployee(int idE)
        {
            //throw new NotImplementedException();
            try
            {
                Employee empToDelete = (from employee in data.Employees where employee.empID == idE select employee).Single();
                data.Employees.DeleteOnSubmit(empToDelete);
                data.SubmitChanges();
                return true;
            }
            catch 
            {

                return false;
            }
        }

        //public string GetData(int value)
        //{
        //    return string.Format("You entered: {0}", value);
        //}

        //public CompositeType GetDataUsingDataContract(CompositeType composite)
        //{
        //    if (composite == null)
        //    {
        //        throw new ArgumentNullException("composite");
        //    }
        //    if (composite.BoolValue)
        //    {
        //        composite.StringValue += "Suffix";
        //    }
        //    return composite;
        //}

        public List<Employee> GetProductList()
        {
            try
            {
                return (from emp in data.Employees select emp).ToList();

            }
            catch
            {

                return null;
            }
        }

        public bool UpdateEmployee(Employee eml)
        {
            //throw new NotImplementedException();
            Employee empToModify = (from emp in data.Employees where emp.empID == eml.empID select emp).Single();
            empToModify.age = eml.age;
            empToModify.empAddress = eml.empAddress;
            empToModify.firstName = eml.firstName;
            empToModify.lastName = eml.lastName;
            data.SubmitChanges();
            return true;
        }
    }
}
