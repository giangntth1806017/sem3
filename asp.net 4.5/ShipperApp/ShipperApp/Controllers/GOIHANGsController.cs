﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ShipperApp.Models;

namespace ShipperApp.Controllers
{
    public class GOIHANGsController : Controller
    {
        private ShipperEntities db = new ShipperEntities();

        // GET: GOIHANGs
        public ActionResult Index()
        {
            var gOIHANGs = db.GOIHANGs.Include(g => g.KHACHHANG);
            return View(gOIHANGs.ToList());
        }

        // GET: GOIHANGs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GOIHANG gOIHANG = db.GOIHANGs.Find(id);
            if (gOIHANG == null)
            {
                return HttpNotFound();
            }
            return View(gOIHANG);
        }

        // GET: GOIHANGs/Create
        public ActionResult Create()
        {
            ViewBag.KH_ID = new SelectList(db.KHACHHANGs, "KHACHHANG_ID", "FIRSTNAME");
            return View();
        }

        // POST: GOIHANGs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "GOIHANG_ID,NAME,CREATED_AT,UPDATED_AT,REMOVED_AT,STATUS,WIEGHT,KH_ID")] GOIHANG gOIHANG)
        {
            if (ModelState.IsValid)
            {
                db.GOIHANGs.Add(gOIHANG);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.KH_ID = new SelectList(db.KHACHHANGs, "KHACHHANG_ID", "FIRSTNAME", gOIHANG.KH_ID);
            return View(gOIHANG);
        }

        // GET: GOIHANGs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GOIHANG gOIHANG = db.GOIHANGs.Find(id);
            if (gOIHANG == null)
            {
                return HttpNotFound();
            }
            ViewBag.KH_ID = new SelectList(db.KHACHHANGs, "KHACHHANG_ID", "FIRSTNAME", gOIHANG.KH_ID);
            return View(gOIHANG);
        }

        // POST: GOIHANGs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "GOIHANG_ID,NAME,CREATED_AT,UPDATED_AT,REMOVED_AT,STATUS,WIEGHT,KH_ID")] GOIHANG gOIHANG)
        {
            if (ModelState.IsValid)
            {
                db.Entry(gOIHANG).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.KH_ID = new SelectList(db.KHACHHANGs, "KHACHHANG_ID", "FIRSTNAME", gOIHANG.KH_ID);
            return View(gOIHANG);
        }

        // GET: GOIHANGs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GOIHANG gOIHANG = db.GOIHANGs.Find(id);
            if (gOIHANG == null)
            {
                return HttpNotFound();
            }
            return View(gOIHANG);
        }

        // POST: GOIHANGs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            GOIHANG gOIHANG = db.GOIHANGs.Find(id);
            db.GOIHANGs.Remove(gOIHANG);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
