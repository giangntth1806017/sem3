﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ShipperApp.Models;

namespace ShipperApp.Controllers
{
    public class DONHANGsController : Controller
    {
        private ShipperEntities db = new ShipperEntities();

        // GET: DONHANGs
        public ActionResult Index()
        {
            var dONHANGs = db.DONHANGs.Include(d => d.GOIHANG).Include(d => d.KHACHHANG).Include(d => d.LOAIDICHVU).Include(d => d.NHANVIEN);
            return View(dONHANGs.ToList());
        }

        // GET: DONHANGs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DONHANG dONHANG = db.DONHANGs.Find(id);
            if (dONHANG == null)
            {
                return HttpNotFound();
            }
            return View(dONHANG);
        }

        // GET: DONHANGs/Create
        public ActionResult Create()
        {
            ViewBag.GOIHANG_ID = new SelectList(db.GOIHANGs, "GOIHANG_ID", "NAME");
            ViewBag.KH_ID = new SelectList(db.KHACHHANGs, "KHACHHANG_ID", "FIRSTNAME");
            ViewBag.LDV_ID = new SelectList(db.LOAIDICHVUs, "LDV_ID", "NAME");
            ViewBag.NV_ID = new SelectList(db.NHANVIENs, "NHANVIEN_ID", "FIRSTNAME");
            return View();
        }

        // POST: DONHANGs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "DONHANG_ID,CREATED_AT,UPDATED_AT,REMOVED_AT,STATUS,TOTAL,NV_ID,LDV_ID,GOIHANG_ID,KH_ID")] DONHANG dONHANG)
        {
            if (ModelState.IsValid)
            {
                db.DONHANGs.Add(dONHANG);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.GOIHANG_ID = new SelectList(db.GOIHANGs, "GOIHANG_ID", "NAME", dONHANG.GOIHANG_ID);
            ViewBag.KH_ID = new SelectList(db.KHACHHANGs, "KHACHHANG_ID", "FIRSTNAME", dONHANG.KH_ID);
            ViewBag.LDV_ID = new SelectList(db.LOAIDICHVUs, "LDV_ID", "NAME", dONHANG.LDV_ID);
            ViewBag.NV_ID = new SelectList(db.NHANVIENs, "NHANVIEN_ID", "FIRSTNAME", dONHANG.NV_ID);
            return View(dONHANG);
        }

        // GET: DONHANGs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DONHANG dONHANG = db.DONHANGs.Find(id);
            if (dONHANG == null)
            {
                return HttpNotFound();
            }
            ViewBag.GOIHANG_ID = new SelectList(db.GOIHANGs, "GOIHANG_ID", "NAME", dONHANG.GOIHANG_ID);
            ViewBag.KH_ID = new SelectList(db.KHACHHANGs, "KHACHHANG_ID", "FIRSTNAME", dONHANG.KH_ID);
            ViewBag.LDV_ID = new SelectList(db.LOAIDICHVUs, "LDV_ID", "NAME", dONHANG.LDV_ID);
            ViewBag.NV_ID = new SelectList(db.NHANVIENs, "NHANVIEN_ID", "FIRSTNAME", dONHANG.NV_ID);
            return View(dONHANG);
        }

        // POST: DONHANGs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "DONHANG_ID,CREATED_AT,UPDATED_AT,REMOVED_AT,STATUS,TOTAL,NV_ID,LDV_ID,GOIHANG_ID,KH_ID")] DONHANG dONHANG)
        {
            if (ModelState.IsValid)
            {
                db.Entry(dONHANG).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.GOIHANG_ID = new SelectList(db.GOIHANGs, "GOIHANG_ID", "NAME", dONHANG.GOIHANG_ID);
            ViewBag.KH_ID = new SelectList(db.KHACHHANGs, "KHACHHANG_ID", "FIRSTNAME", dONHANG.KH_ID);
            ViewBag.LDV_ID = new SelectList(db.LOAIDICHVUs, "LDV_ID", "NAME", dONHANG.LDV_ID);
            ViewBag.NV_ID = new SelectList(db.NHANVIENs, "NHANVIEN_ID", "FIRSTNAME", dONHANG.NV_ID);
            return View(dONHANG);
        }

        // GET: DONHANGs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DONHANG dONHANG = db.DONHANGs.Find(id);
            if (dONHANG == null)
            {
                return HttpNotFound();
            }
            return View(dONHANG);
        }

        // POST: DONHANGs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            DONHANG dONHANG = db.DONHANGs.Find(id);
            db.DONHANGs.Remove(dONHANG);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
