﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ShipperApp.Models;

namespace ShipperApp.Controllers
{
    public class LOAIDICHVUsController : Controller
    {
        private ShipperEntities db = new ShipperEntities();

        // GET: LOAIDICHVUs
        public ActionResult Index()
        {
            return View(db.LOAIDICHVUs.ToList());
        }

        // Get Action method for handling AJAX POST operation
        [HttpPost]
        public JsonResult AjaxMethod()
        {
            //string query = "select NAME, HESOTIEN from LOAIDICHVU";
            //query += " FROM Orders WHERE ShipCountry = 'USA' GROUP BY ShipCity";
            //string constr = ConfigurationManager.ConnectionStrings["ShipperEntities"].ConnectionString;
            List<object> chartData = new List<object>();
            chartData.Add(new object[]
                            {
                            "NAME", "HESOTIEN"
                            });
            //List<LOAIDICHVU> ldvResult = new List<LOAIDICHVU>();
            foreach (var item in db.LOAIDICHVUs.ToList())
            {
                chartData.Add(new object[] 
                {
                    item.NAME, item.HESOTIEN
                });
            } 
            //using (SqlConnection con = new SqlConnection(constr))
            //{
            //    using (SqlCommand cmd = new SqlCommand(query))
            //    {
            //        cmd.CommandType = CommandType.Text;
            //        cmd.Connection = con;
            //        con.Open();
            //        using (SqlDataReader sdr = cmd.ExecuteReader())
            //        {
            //            while (sdr.Read())
            //            {
            //                chartData.Add(new object[]
            //                {
            //                sdr["NAME"], sdr["HESOTIEN"]
            //                });
            //            }
            //        }

            //        con.Close();
            //    }
            //}
            return Json(chartData);
            //return View();
        }

        // GET: LOAIDICHVUs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LOAIDICHVU lOAIDICHVU = db.LOAIDICHVUs.Find(id);
            if (lOAIDICHVU == null)
            {
                return HttpNotFound();
            }
            return View(lOAIDICHVU);
        }

        // GET: LOAIDICHVUs/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: LOAIDICHVUs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "LDV_ID,NAME,CREATED_AT,UPDATED_AT,REMOVED_AT,STATUS,MOTA,HESOTIEN")] LOAIDICHVU lOAIDICHVU)
        {
            if (ModelState.IsValid)
            {
                db.LOAIDICHVUs.Add(lOAIDICHVU);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(lOAIDICHVU);
        }

        // GET: LOAIDICHVUs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LOAIDICHVU lOAIDICHVU = db.LOAIDICHVUs.Find(id);
            if (lOAIDICHVU == null)
            {
                return HttpNotFound();
            }
            return View(lOAIDICHVU);
        }

        // POST: LOAIDICHVUs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "LDV_ID,NAME,CREATED_AT,UPDATED_AT,REMOVED_AT,STATUS,MOTA,HESOTIEN")] LOAIDICHVU lOAIDICHVU)
        {
            if (ModelState.IsValid)
            {
                db.Entry(lOAIDICHVU).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(lOAIDICHVU);
        }

        // GET: LOAIDICHVUs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LOAIDICHVU lOAIDICHVU = db.LOAIDICHVUs.Find(id);
            if (lOAIDICHVU == null)
            {
                return HttpNotFound();
            }
            return View(lOAIDICHVU);
        }

        // POST: LOAIDICHVUs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            LOAIDICHVU lOAIDICHVU = db.LOAIDICHVUs.Find(id);
            db.LOAIDICHVUs.Remove(lOAIDICHVU);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
