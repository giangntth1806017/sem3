﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShipperApp.Models
{
    public interface IEntity
    {
        //int Id { get; set; }
        DateTime? CREATED_AT { get; set; }
        DateTime? UPDATED_AT { get; set; }
        DateTime? REMOVED_AT { get; set; }
    }

    public partial class LOAIDICHVU : IEntity { }
}
