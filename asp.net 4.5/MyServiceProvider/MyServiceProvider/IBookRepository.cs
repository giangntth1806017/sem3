﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyServiceProvider
{
    public interface IBookRepository
    {
        List<Book> GetBooks();
        Book AddNewBook(Book book);
    }
}
