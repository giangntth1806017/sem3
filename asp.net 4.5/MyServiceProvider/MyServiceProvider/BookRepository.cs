﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyServiceProvider
{
    public class BookRepository : IBookRepository
    {
        private List<Book> books = new List<Book>();
        private int counter = 1;

        public BookRepository()
        {
            AddNewBook(new Book() { Title="AAA", ISBN="A01_2019"});
            AddNewBook(new Book() { Title="BBB", ISBN="B02_2019"});
        }

        public Book AddNewBook(Book book)
        {
            if (book == null)
            {
                throw new NotImplementedException();
            }
            book.BookId = counter++;
            books.Add(book);
            return book;
            
        }

        public List<Book> GetBooks()
        {
            return books;
            //throw new NotImplementedException();
        }
    }
}