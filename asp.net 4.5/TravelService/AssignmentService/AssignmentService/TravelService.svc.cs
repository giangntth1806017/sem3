﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Web;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Hosting;

namespace AssignmentService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "TravelService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select TravelService.svc or TravelService.svc.cs at the Solution Explorer and start debugging.
    public class TravelService : ITravelService
    {
        TravelDataDataContext data = new TravelDataDataContext();

        public bool AddComment(int place_id, int people_id, string comment, string time)
        {
            try
            {
                data.PlaceComments.InsertOnSubmit(new PlaceComment
                {
                    comment = comment,
                    place_id = place_id,
                    user_id = people_id,
                    time = time
                });
                data.SubmitChanges();
                return true;
            }
            catch (Exception ex)
            {

            }
            return false;
        }

        public bool AddCustomer(Customer people)
        {
            try
            {
                data.Customers.InsertOnSubmit(people);
                data.SubmitChanges();

                return true;
            }
            catch (Exception e)
            {
                return false;
            }

        }

        public bool AddPlace(Place place)
        {
            try
            {
                data.Places.InsertOnSubmit(place);
                data.SubmitChanges();
                return true;

            }
            catch (Exception e)
            {
                return false;
            }
        }
        public void UploadFile(string fileName, Stream stream)
        {
            string FilePath = Path.Combine(HostingEnvironment.MapPath("~/Assets/Uploads"), fileName);
            int length = 0;
            using (FileStream writer = new FileStream(FilePath, FileMode.Create))
            {
                int readCount;
                var buffer = new byte[8192];
                while ((readCount = stream.Read(buffer, 0, buffer.Length)) != 0)
                {
                    writer.Write(buffer, 0, readCount);
                    length += readCount;
                }
            }
        }
        public bool AddThumbnail(HttpPostedFileBase file, int id, string time)
        {
            try
            {
                var fileName = CurrentTime() + Path.GetFileName(file.FileName);
                UploadFile(fileName, file.InputStream);

                string FilePath = Path.Combine(HostingEnvironment.MapPath("~/Assets/Uploads"), fileName);
                data.PlaceThumbnails.InsertOnSubmit(new PlaceThumbnail
                {
                    place_id = id,
                    time = time,
                    src = FilePath
                });
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool CheckInfor(Customer people)
        {
            try
            {
                var p = data.Customers.Where(it => it.email == people.email && it.password == people.password).Single();
                if (p != null)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
            }
            return false;
        }

        public bool DeleteCustomer(int id)
        {
            try
            {
                var p = data.Customers.Where(it => it.id == id).Single();
                if (p != null)
                {
                    data.Customers.DeleteOnSubmit(p);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
            }
            return false;
        }

        public bool DeletePlace(int id)
        {
            throw new NotImplementedException();
        }

        public Customer GetInfor(string email)
        {
            throw new NotImplementedException();
        }

        public List<Place> GetPlace()
        {
            try
            {
                return data.Places.ToList();
            }
            catch (Exception e)
            {
                return new List<Place>();
            }
        }


        public bool UpdateCustomer(Customer people)
        {
            try
            {
                var c = data.Customers.Where(it => it.id == people.id).Single();
                if (c != null)
                {
                    c.name = people.name;
                    c.password = people.password;
                    c.thumbnail = people.thumbnail;
                    c.address = people.address;
                    c.email = people.email;
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                return false;
            }
        }


        private long CurrentTime()
        {
            var Jan1st1970 = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            return (long)(DateTime.UtcNow - Jan1st1970).TotalMilliseconds;
        }
        public string ToJson(object input)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            return serializer.Serialize(input);
        }

        public bool AddEmployee(Employee employee)
        {
            try
            {
                data.Employees.InsertOnSubmit(employee);
                data.SubmitChanges();

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public bool DeleteEmployee(Employee employee, int id)
        {
            try
            {
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public bool UpdateEmployee(Employee employee)
        {
            try
            {
                var e = data.Employees.Where(it => employee.id == it.id).Single();
                if (e != null)
                {
                    e.name = employee.name;
                    e.password = employee.password;
                    e.thumbnail = employee.thumbnail;
                    e.address = employee.address;
                    data.SubmitChanges();
                    return true;
                }
                else
                {
                    return false;
                }


            }
            catch (Exception e)
            {
                return false;
            }
        }

        public bool UpdatePlace(Place place, int employee_id)
        {
            try
            {
                var p = data.Places.Where(it => it.id == place.id).Single();

                if (p != null && p.add_id == employee_id)
                {
                    if (place.rate != 0)
                    {
                        p.rate = place.rate;
                    }
                    p.name = place.name;
                    p.scope = place.scope;
                    p.time = place.time;
                    p.address = place.address;
                    p.description = place.description;

                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public bool DeletePlace(int place_id, int employee_id)
        {
            try
            {
                var p = data.Places.Where(it => it.id == place_id).Single();
                if (p != null && p.add_id == employee_id)
                {
                    data.Places.DeleteOnSubmit(p);
                    data.SubmitChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                return false;

            }
        }

        public bool AddPlace(Place place, int employee_id)
        {
            try
            {
                var e = data.Employees.Where(it => it.id == employee_id).Single();
                if (e != null)
                {
                    data.Places.InsertOnSubmit(place);
                    data.SubmitChanges();
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception e)
            {
                return false;
            }
        }

        public string UploadFile(HttpPostedFileBase file)
        {
            var fileName = CurrentTime() + Path.GetFileName(file.FileName);
            UploadFile(fileName, file.InputStream);

            string FilePath = Path.Combine(HostingEnvironment.MapPath("~/Assets/Uploads"), fileName);
            return FilePath;
        }
    }
}
