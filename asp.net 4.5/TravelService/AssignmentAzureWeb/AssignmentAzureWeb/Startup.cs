﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AssignmentAzureWeb.Startup))]
namespace AssignmentAzureWeb
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
