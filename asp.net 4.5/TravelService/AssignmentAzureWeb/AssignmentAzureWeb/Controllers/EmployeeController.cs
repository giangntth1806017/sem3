﻿using AssignmentAzureWeb.TravelService;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace AssignmentAzureWeb.Controllers
{
    public class EmployeeController : Controller
    {
        // GET: Employee


        TravelServiceClient webService = new TravelServiceClient();

        public ActionResult Login()
        {
            return View("~/Views/Employee/Login.cshtml");
        }
        public ActionResult Register()
        {
            return View("~/Views/Employee/Register.cshtml");
        }
        [HttpPost]
        public async Task<ActionResult> Register(Employee employee)
        {
            if (employee != null)
            {
                var e = new Employee
                {
                    name = employee.name,
                    password = employee.password,
                    thumbnail = "",
                    email = employee.email,
                    address = employee.address
                };
                var returnEmployee = await webService.AddEmployeeAsync(e);

                Debug.WriteLine("Thien: " + returnEmployee);
                if (returnEmployee)
                {
                    return RedirectToAction("Login", "Employee");
                }
            }
            return View("~/Views/Employee/Register.cshtml");
        }

        public ActionResult Index()
        {
            return View();
        }

        public bool checkEmployee()
        {
            var employee = Session["employee"] as Employee;
            if (employee != null)
            {
                return true;
            }

            return false;

        }
    }
}